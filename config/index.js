import dotenv from 'dotenv';

dotenv.config();

const commonConfig = {
  JWT_SECRET: process.env.JWT_SECRET || 'default_secret',
};

const developmentConfig = {
  PORT: process.env.PORT || 5000,
  CLIENT_PORT: process.env.CLIENT_PORT || 3000,
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost:27017',
  MONGO_DB_NAME: process.env.MONGO_DB_NAME || 'development_db',
};

const productionConfig = {
  PORT: process.env.PORT || 5000,
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost:27017',
  MONGO_DB_NAME: process.env.MONGO_DB_NAME || 'production_db',
};

const config = process.env.NODE_ENV === 'production'
  ? { ...commonConfig, ...productionConfig }
  : { ...commonConfig, ...developmentConfig };

export default config;
