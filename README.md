# MERN Supply Chain

> Supply Chain list app built with the MERN stack along with Redux for state management, Reactstrap and react-transition-group.

## Quick Start

Add your MONGO_URI to the default.json file. Make sure you set an env var for that and the jwtSecret on deployment

```bash
# Install dependencies for server
yarn install

# Install dependencies for client
yarn --cwd client install

# Run the client & server with concurrently
yarn dev

# Run the Express server only - chainge later
yarn server

# Run the React client only - changer later
yarn client

# Server runs on http://localhost:5000 and client on http://localhost:3000
```

## Deployment

There is a Heroku post build script so that you do not have to compile your React frontend manually, it is done on the server. Simply push to Heroku and it will build and load the client index.html page

## App Info

### Author

People Health / adapted from Brad Traversy
[People Health](http://www.people.health)
[Traversy Media](http://www.traversymedia.com)

### Version

2.0.0

### License

This project is licensed under the MIT License
