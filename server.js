import app from './app.js';
import config from './config/index.js';

const { PORT, CLIENT_PORT } = config;

//console.log(config);

console.log(`PORT: ${process.env.PORT}`);
console.log(`CLIENT_PORT: ${process.env.CLIENT_PORT}`);

//app.listen(PORT, '0.0.0.0', () => console.log(`Server started on PORT ${PORT}`));

const server = app.listen(PORT, '0.0.0.0', () => {
  const pid = process.pid;
  console.log(`Server started on PORT ${PORT} (PID: ${pid})`);
});
  
process.on('SIGINT', () => {
  server.close(() => {
    console.log('Server stopped');
    process.exit(0);
  });
});
