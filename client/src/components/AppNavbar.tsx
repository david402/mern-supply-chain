import React, { Fragment, useState } from 'react';
import NavItem from 'reactstrap/lib/NavItem';
import Container from 'reactstrap/lib/Container';
import NavbarToggler from 'reactstrap/lib/NavbarToggler';
import Navbar from 'reactstrap/lib/Navbar';
import Nav from 'reactstrap/lib/Nav';
import NavbarBrand from 'reactstrap/lib/NavbarBrand';
import Collapse from 'reactstrap/lib/Collapse';

import { connect } from 'react-redux';
import RegisterModal from './auth/RegisterModal';
import LoginModal from './auth/LoginModal';
import Logout from './auth/Logout';
//import { IAppNavbar } from '../types/interfaces';
import { IAppNavbar, IAuthReduxProps, IUser } from '../types/interfaces';

/* interface IAppNavbar {
  auth?: {
    isAuthenticated: boolean;
    user?: IUser;
  };
} */

//const AppNavbar = ({ auth }: IAppNavbar) => {
//  const [isOpen, setIsOpen] = useState(false);

  const AppNavbar: React.FC<IAppNavbar> = ({ auth }) => {
    const [isOpen, setIsOpen] = useState(false);  

  const handleToggle = () => setIsOpen(!isOpen);

  const authLinks = (
    <Fragment>
      <NavItem>
        <span className="navbar-text mr-3">
          <strong>
            {auth && auth.user ? `Welcome ${auth.user.name}` : ''}
          </strong>
        </span>
      </NavItem>
      <NavItem>
        <Logout />
      </NavItem>
    </Fragment>
  );

  const guestLinks = (
    <Fragment>
      <NavItem>
        <RegisterModal />
      </NavItem>
      <NavItem>
        <LoginModal />
      </NavItem>
    </Fragment>
  );

  return (
    <div>
      <Navbar color="dark" dark expand="sm" className="mb-5">
        <Container>
          <NavbarBrand href="/">Supply Chain</NavbarBrand>
          <NavbarToggler onClick={handleToggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {auth && auth.isAuthenticated ? authLinks : guestLinks}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

const mapStateToProps = (state: IAuthReduxProps) => ({
  auth: state.auth,
});

//export default connect(mapStateToProps, null)(AppNavbar);
export default connect(mapStateToProps)(AppNavbar);
