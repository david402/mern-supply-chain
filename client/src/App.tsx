import React, { useEffect } from 'react';
import AppNavbar from './components/AppNavbar';
import SupplyChain from './components/SupplyChain';
import ItemModal from './components/ItemModal';
import Container from 'reactstrap/lib/Container';

import { Provider } from 'react-redux';
import store from './flux/store';
import { loadUser } from './flux/actions/authActions';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <div className="App">
        <AppNavbar />
        <Container>
          <ItemModal />
          <SupplyChain />
        </Container>
      </div>
    </Provider>
  );
};

export default App;
