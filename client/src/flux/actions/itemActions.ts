import axios from 'axios';
import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from './types';
import { tokenConfig } from './authActions';
import { returnErrors } from './errorActions';
import { IItem } from '../../types/interfaces';

export const getItems = () => (dispatch: Function) => {
  dispatch(setItemsLoading());
  axios
    .get('/api/items')
    .then(res =>
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const addItem = (item: IItem, callback: () => void) => (
  dispatch: Function,
  getState: Function
) => {
  console.log('Item to be added:', item); // Log the item object
  console.log('Current state:', getState()); // Log the current state
  axios
    .post('/api/items', item, tokenConfig(getState))
    .then(res => {
      console.log('Before dispatching ADD_ITEM');
      dispatch({
        type: ADD_ITEM,
        payload: res.data
      });
      console.log('Item added:', res.data); // Log the added item
      callback(); // Fetch the updated item list after adding an item
    })
    .catch(err => {
      console.log('Error adding next item:', err); // Log any errors
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const deleteItem = (id: string) => (
  dispatch: Function,
  getState: Function
) => {
  axios
    .delete(`/api/items/${id}`, tokenConfig(getState))
    .then(res =>
      dispatch({
        type: DELETE_ITEM,
        payload: id
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setItemsLoading = () => {
  return {
    type: ITEMS_LOADING
  };
};
